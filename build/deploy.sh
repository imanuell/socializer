#!/bin/sh
#
# Deploy script
#
set -e # Bail out on errors
set -u # Bail out on undefined variables
set -v # verbose on

if [ $# -eq 3 ]; then
    ENV="$1"
    RELEASE_NAME="$2"
    RELEASE_VERSION="$3"
else
    echo 'Error: version argument required!'
    exit 1
fi

DEPLOY_DIR="/home/innovid"
RELEASE_DIR="${DEPLOY_DIR}/build-${RELEASE_NAME}"

if [ -f /etc/debian_version ]; then
    APACHE_USER="www-data"
elif [ -f /etc/redhat-release ]; then
    APACHE_USER="innovid"
fi

mkdir -p $DEPLOY_DIR
cd $DEPLOY_DIR

chown -R ${APACHE_USER}:innovid $RELEASE_DIR/socializer/
cd ${DEPLOY_DIR}/public_html
rm -rf socializer
ln -s ${RELEASE_DIR}/socializer/client ./socializer
rm -rf socializer-service
ln -s ${RELEASE_DIR}/socializer/service/web ./socializer-service
