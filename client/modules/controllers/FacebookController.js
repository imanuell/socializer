mainApp.controller('FacebookController', function($scope, $http, fileReader, FACEBOOK_SERVICE_URL) {

    $scope.getTag = function() {
        var data = {
            project: $scope.name,
            title: $scope.title,
            description: $scope.description,
            flash: $scope.flash,
            html: $scope.html,
            img: $scope.imageSrc
        };

        $scope.sending = true;

        $http.post(FACEBOOK_SERVICE_URL,data).then(function(response){
                updateEmbedTag(response);
            }
        );
    };

    $scope.getFile = function () {
        var reader = new FileReader();
        reader.onload = function(e) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function(){
                var canvas = document.createElement('CANVAS'),
                    ctx = canvas.getContext('2d'), dataURL;
                canvas.height = this.height;
                canvas.width = this.width;
                ctx.drawImage(this, 0, 0);
                dataURL = canvas.toDataURL();
                canvas = null;
                $scope.imageSrc = dataURL;
            };
            img.src = e.target.result;
        };
        reader.readAsDataURL($scope.file);
    };

    function updateEmbedTag(response) {
        if(response.data.html) {
            $scope.embedTag = response.data.html;
        }
        $scope.sending = false;
    }

});
