var mainApp = angular.module('mainApp', ['configuration']);

//
// Routes:
//
mainApp.config(function($routeProvider) {
    $routeProvider.
        when('/facebook', {
            controller: 'FacebookController', templateUrl: 'modules/views/facebook.html', access: { restricted: false }
        });
});

mainApp.directive('customOnChange', function() {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var onChangeHandler = scope.$eval(attrs.customOnChange);
            element.bind('change', onChangeHandler);
        }
    };
});

mainApp.directive("ngFileSelect",function(){
    return {
        link: function($scope,el){
            el.bind("change", function(e){
                $scope.file = (e.srcElement || e.target).files[0];
                $scope.getFile();
            })
        }
    }
});





