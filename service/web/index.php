<?php

require_once '../vendor/autoload.php';
require_once '../autoload.php';
spl_autoload_register('autoload');


use Silex\Application;
use Igorw\Silex\ConfigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Socializer\Controller\FacebookController;

//
// Bootstrap
//

$app = new Application();

// The INNOVID_ENV should store the env id (e.g. 'prod')
if (isset($_SERVER['INNOVID_ENV'])) {
    $env = $_SERVER['INNOVID_ENV'];
} else {
    $env = 'prod';
}

$envConfigFile = $confDir . $env . ".json";

if (file_exists($envConfigFile)) {
    $app->register(new ConfigServiceProvider($envConfigFile));
}

$app['selectedEnv'] = $env;

$app->register(new ServiceControllerServiceProvider());


//
// Routes
//

$app->match('{anything}', function (Application $app) {

    return new Response("", 200, array(
        'Access-Control-Allow-Methods' => 'GET, PUT, POST',
        'Access-Control-Allow-Headers' => 'Accept, Origin, X-Requested-With, Content-Type'
    ));

})->method('OPTIONS');


$app['FacebookController'] = $app->share(function() use ($app) {
    return new FacebookController($app);
});

$app->post('/facebook', "FacebookController:getEmbedTag");

$app->after(function (Request $request, Response $response) {
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->headers->set('Access-Control-Allow-Credentials', 'true');
    $response->headers->set('Access-Control-Max-Age', '0');
});

$app->run();
