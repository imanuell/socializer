<?php
namespace Socializer\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use \Silex\Application;
use Aws;

class FacebookController {

    private $app;

    private $htmlFileName;

    private $imageFileName;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getEmbedTag(Request $request) {

        $requestContent = json_decode($request->getContent());

        $s3 = \Aws\S3\S3Client::factory(
            array(
                'key'    => 'AKIAI3UTKDTCCKP2X3IA',
                'secret' => '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'
            )
        );

        $bucketName = 'static.innovid.com';
        $path = '/social/facebook/';

        $fileName = htmlspecialchars($requestContent->project) . '_' . md5(openssl_random_pseudo_bytes(20));

        $this->htmlFileName = $fileName  . ".html";
        $this->imageFileName = $fileName  . ".jpg";

        $this->uploadImage($requestContent->img,  $this->imageFileName);

        $data = new \stdClass();
        $data->project = trim(htmlspecialchars($requestContent->project));
        $data->title = trim(htmlspecialchars($requestContent->title));
        $data->description = trim(htmlspecialchars($requestContent->description));
        $data->flash = trim(htmlspecialchars($requestContent->flash));
        $data->html = trim(htmlspecialchars($requestContent->html));

        $html_file = $this->generateHtmlFile($data);

        try {
            $s3->putObject(array(
                    'Bucket' => $bucketName,
                    'Key' => $path . baseName($this->imageFileName),
                    'SourceFile' => '/tmp/' . $this->imageFileName,
                    'ContentType' => 'image/jpeg',
                    'ACL' => 'public-read'
                )
            );

            $s3->putObject(array(
                    'Bucket' => $bucketName,
                    'Key' => $path . baseName($this->htmlFileName),
                    'Body' => $html_file,
                    'ContentType' => 'text/html',
                    'ACL' => 'public-read'
                )
            );

            $response = new \stdClass();
            $response->html = 'http://' . $bucketName . $path . baseName($this->htmlFileName);
            $response->image = 'http://' . $bucketName . $path . baseName($this->imageFileName);



            return new Response(json_encode($response), 200);
        }

        catch (\Aws\S3\Exception\S3Exception $e)
        {
            return new Response($e->getMessage());
        }
    }

    private function generateHtmlFile($data) {

        $bucketName = 'static.innovid.com';
        $path = '/social/facebook/';

        $fileLocation = "http://" . $bucketName . $path . baseName($this->htmlFileName);

        $htmlTemplate = file_get_contents('../Templates/facebookEmbedTemplate.txt', true);
        $htmlTemplate = str_replace("##PAGE_TITLE##", $data->project , $htmlTemplate);
        $htmlTemplate = str_replace("##OG_TITLE##", $data->title , $htmlTemplate);
        $htmlTemplate = str_replace("##OG_DESCRIPTION##", $data->description , $htmlTemplate);
        $htmlTemplate = str_replace("##FLASH_TAG##", $data->flash , $htmlTemplate);
        $htmlTemplate = str_replace("##HTML_TAG##", $data->html , $htmlTemplate);
        $htmlTemplate = str_replace("##FILE_LOCATION##", $fileLocation , $htmlTemplate);
        $htmlTemplate = str_replace("##IMG_URL##", 'http://' . $bucketName . $path . baseName($this->imageFileName) , $htmlTemplate);

        return $htmlTemplate;
    }

    private function uploadImage($imageData, $fileName) {

        if ( isset ( $imageData)){
            try
            {
                $fp = fopen('/tmp/' . $fileName, "wb");
                $data = explode(',', $imageData);
                fwrite($fp, base64_decode($data[1]));
                fclose($fp);
            }
            catch(Exception $e)
            {
                echo '{"status":"error", "reason":"cannot create image file"}';
                exit;
            }
        }
        else
        {
            echo '{"status":"error", "reason":"no file"}';
            exit;
        }
    }

};
