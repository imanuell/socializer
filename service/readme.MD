# Feature Bits Service Demo

This is a small PHP feature bit service with an .ini backend.

## Build

To build the service, run:

php composer.phar install

in the base dir of the service.

## Deployment

The service is designed to work with the apache web server and PHP, where the document root is the web folder.

In the host configuration, please specify the APP_ENV environment variable to 'prod' or 'dev'.
In case of 'prod', the app will be in a read-only mode and debug will be disabled.
